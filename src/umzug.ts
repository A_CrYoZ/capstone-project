import { Umzug, SequelizeStorage } from "umzug";
import { Sequelize } from "sequelize";
import { config } from "./config";

const sequelize = new Sequelize({
  ...config.db,
});

export const migrator = new Umzug({
  migrations: {
    glob: ["./migrations/*.ts", { cwd: __dirname }],
  },
  context: sequelize,
  storage: new SequelizeStorage({
    sequelize,
  }),
  logger: console,
});

export type Migration = typeof migrator._types.migration;

export const seeder = new Umzug({
  migrations: {
    glob: ["./seeders/*.ts", { cwd: __dirname }],
  },
  context: sequelize,
  storage: new SequelizeStorage({
    sequelize,
    modelName: "SeederMeta",
  }),
  logger: console,
});

export type Seeder = typeof seeder._types.migration;
