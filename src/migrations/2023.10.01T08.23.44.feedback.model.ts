import type { MigrationFn } from "umzug";
import { DataTypes, type Sequelize } from "sequelize";

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q
    .createTable("feedbacks", {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      from_user: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        onDelete: "cascade",
        references: {
          model: "users",
          key: "id",
        },
      },
      to_user: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        onDelete: "cascade",
        references: {
          model: "users",
          key: "id",
        },
      },
      content: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      company_name: {
        type: DataTypes.STRING(128),
        allowNull: false,
      },
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    })
    .then(async () => {
      await q.addIndex("feedbacks", ["company_name"]);
    });
};
export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.dropTable("feedbacks");
};
