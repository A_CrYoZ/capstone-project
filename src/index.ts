import { loadApp } from "./loaders/app";
import { logger } from "./libs/logger";

(() => {
  loadApp().then(
    (app) => {
      app.listen(3001, () => {
        logger.info(`Application is running on http://localhost:3001`);
      });
    },
    (error) => {
      logger.fatal(error);
    },
  );
})();
