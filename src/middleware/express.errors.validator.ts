import { type NextFunction, type Response } from "express";
import { validationResult } from "express-validator";
import createHttpError from "http-errors";

import { ErrorMsgs } from "../interfaces/general";
import { logger } from "../libs/logger";
import { type ExtendedRequest } from "../interfaces/express";

export default (
  req: ExtendedRequest,
  res: Response,
  next: NextFunction,
): void => {
  const errors = validationResult(req);

  if (errors.isEmpty()) {
    next();
    return;
  }

  logger.info(
    {
      requestId: req.id,
      file: req.file,
      errors: errors.array(),
    },
    "Validation errors array",
  );

  next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
};
