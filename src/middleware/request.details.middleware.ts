import { type NextFunction, type Response } from "express";

import { logger } from "../libs/logger";
import { type ExtendedRequest } from "../interfaces/express";

export default (
	req: ExtendedRequest,
	res: Response,
	next: NextFunction
): void => {
	logger.info(
		{
			method: req.method,
			path: req.path,
			queryParams: req.query,
			headers: { ...req.headers, authorization: undefined },
			user: req.user,
		},
		"Request info"
	);

	next();
};
