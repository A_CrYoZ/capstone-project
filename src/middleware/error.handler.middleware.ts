import { type NextFunction, type Response } from "express";
import { logger } from "../libs/logger";
import { type ExtendedRequest } from "../interfaces/express";
import { ErrorMsgs } from "../interfaces/general";

export default (
	error: Error & {
		statusCode?: number;
	},
	req: ExtendedRequest,
	res: Response,
	next: NextFunction
): void => {
	const resInfo = error.statusCode
		? { status: error.statusCode, message: error.message }
		: { status: 500, message: ErrorMsgs.INTERNAL_SERVER_ERROR };

	if (resInfo.status === 500) {
		logger.fatal({
			error: {
				message: error.message,
				stack: error.stack,
			},
		});
	} else {
		logger.error({
			error,
		});
	}

	res.status(resInfo.status).json({ message: resInfo.message });
};
