import { type NextFunction, type Response, type Request } from "express";
import createHttpError from "http-errors";
import { ErrorMsgs } from "../interfaces/general";

export default (req: Request, res: Response, next: NextFunction): void => {
  next(createHttpError(404, ErrorMsgs.NOT_FOUND));
};
