import { type NextFunction, type Response } from "express";
import { type ExtendedRequest } from "../interfaces/express";
import createHttpError from "http-errors";
import { ErrorMsgs, UserRole } from "../interfaces/general";
import { type ExperienceService } from "../services/experience.service";
import { type FeedbackService } from "../services/feedback.service";
import { type ProjectsService } from "../services/projects.service";

export const authorizeUser =
  (allowedRole: UserRole[]) =>
  (req: ExtendedRequest, res: Response, next: NextFunction): void => {
    !req.user || !allowedRole.includes(req.user.role)
      ? next(createHttpError(401, "Unauthorized"))
      : next();
  };

// Only user that owns account can update it
export const authorizeUserUpdate = (
  req: ExtendedRequest,
  res: Response,
  next: NextFunction,
): void => {
  if (!req.user) {
    throw new Error(
      "authorizeUserUpdate middleware must be implemented after authentication middleware!",
    );
  }
  if (!req.params.id) {
    throw new Error("Id does not exist in path parameters!");
  }

  const userId = parseInt(req.params.id);

  if (req.user.id !== userId) {
    next(createHttpError(401, "Unauthorized"));
    return;
  }

  next();
};

export const authorizeUserDeletion = (
  req: ExtendedRequest,
  res: Response,
  next: NextFunction,
): void => {
  if (!req.user) {
    throw new Error(
      "authorizeUserDeletion middleware must be implemented after authentication middleware!",
    );
  }
  if (!req.params.id) {
    throw new Error("Id does not exist in path parameters!");
  }

  const userId = parseInt(req.params.id);

  if (req.user.id !== userId && req.user.role !== UserRole.Admin) {
    next(createHttpError(401, ErrorMsgs.UNAUTHORIZED));
    return;
  }

  next();
};

export const authorizeExperience = (
  experienceService: typeof ExperienceService,
) => {
  return async (
    req: ExtendedRequest,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    if (!req.user) {
      throw new Error(
        "authorizeExperience middleware must be implemented after authentication middleware!",
      );
    }
    if (!req.params.id) {
      throw new Error("Id does not exist in path parameters!");
    }

    const { id: expId } = req.params;
    const { id: userId, role: userRole } = req.user;

    if (typeof expId !== "number") {
      next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
      return;
    }

    const exp = await experienceService.findExperienceById(expId);

    if (!exp) {
      next(createHttpError(404, ErrorMsgs.NOT_FOUND));
      return;
    }

    if (userId !== exp.userId && userRole !== UserRole.Admin) {
      next(createHttpError(401, ErrorMsgs.UNAUTHORIZED));
      return;
    }

    next();
  };
};

export const authorizeFeedback = (feedbackService: typeof FeedbackService) => {
  return async (
    req: ExtendedRequest,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    if (!req.user) {
      throw new Error(
        "authorizeFeedback middleware must be implemented after authentication middleware!",
      );
    }
    if (!req.params.id) {
      throw new Error("Id does not exist in path parameters!");
    }

    const { id: feedbackId } = req.params;
    const { id: userId, role: userRole } = req.user;

    if (typeof feedbackId !== "number") {
      next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
      return;
    }

    const feedback = await feedbackService.findFeedbackById(feedbackId);

    if (!feedback) {
      next(createHttpError(404, ErrorMsgs.NOT_FOUND));
      return;
    }

    if (userId !== feedback.fromUser && userRole !== UserRole.Admin) {
      next(createHttpError(401, ErrorMsgs.UNAUTHORIZED));
      return;
    }

    next();
  };
};

export const authorizeProject = (projectsService: typeof ProjectsService) => {
  return async (
    req: ExtendedRequest & { projectImg: string },
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    if (!req.user) {
      throw new Error(
        "authorizeProject middleware must be implemented after authentication middleware!",
      );
    }
    if (!req.params.id) {
      throw new Error("Id does not exist in path parameters!");
    }

    const { id: feedbackId } = req.params;
    const { id: userId, role: userRole } = req.user;

    if (typeof feedbackId !== "number") {
      next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
      return;
    }

    const project = await projectsService.findProjectById(feedbackId);

    if (!project) {
      next(createHttpError(404, ErrorMsgs.NOT_FOUND));
      return;
    }

    if (userId !== project.userId && userRole !== UserRole.Admin) {
      next(createHttpError(401, ErrorMsgs.UNAUTHORIZED));
      return;
    }

    // Add info about project image to request obj
    req.projectImg = project.image;

    next();
  };
};
