import { check } from "express-validator";

export const feedbackParamsValidators = [
	check("fromUser").escape().trim().isNumeric().toInt(10),
	check("toUser").escape().trim().isNumeric().toInt(10),
	check("companyName").escape().trim().isLength({ max: 128 }),
	check("content").escape().trim().isLength({ min: 10 }),
];
