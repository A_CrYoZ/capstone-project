import { check } from "express-validator";

export const experienceParamsValidators = [
	check("userId").escape().trim().isNumeric().toInt(10),
	check("companyName").escape().trim().isLength({ max: 256 }),
	check("role").escape().trim().isLength({ max: 256 }),
	check("startDate").escape().trim().isDate(),
	check("endDate").escape().trim().isDate(),
	check("description").escape().trim().isLength({ min: 10 }),
];
