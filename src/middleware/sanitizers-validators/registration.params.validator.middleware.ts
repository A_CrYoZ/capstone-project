import { check } from "express-validator";

export const registrationParamsValidators = [
	check("email")
		.escape()
		.trim()
		.isEmail()
		.isLength({ min: 6 })
		.withMessage("Email length cannot be less than 6 symbols"),
	check("password")
		.escape()
		.trim()
		.isStrongPassword({ minLength: 8 })
		.withMessage("Weak password"),
	check("firstName").escape().trim().isString().isLength({ min: 2, max: 128 }),
	check("lastName").escape().trim().isString().isLength({ min: 2, max: 128 }),
	check("title").escape().trim().isString().isLength({ min: 2, max: 256 }),
	check("summary").escape().trim().isString().isLength({ min: 2, max: 256 }),
];
