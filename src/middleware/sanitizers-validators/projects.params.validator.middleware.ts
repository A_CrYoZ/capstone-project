import { check } from "express-validator";

export const projectsParamsValidators = [
  check("userId").escape().trim().isNumeric().toInt(10),
  check("description").escape().trim().isLength({ min: 10 }),
];
