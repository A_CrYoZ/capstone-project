import { DataTypes, Model, type Optional, type Sequelize } from "sequelize";
import { type Models } from "../../src/interfaces/general";

export interface FeedbackAttributes {
	id: number;
	fromUser: number;
	toUser: number;
	content: string;
	companyName: string;
}

export class Feedback
	extends Model<FeedbackAttributes, Optional<FeedbackAttributes, "id">>
	implements FeedbackAttributes
{
	id: number;
	fromUser: number;
	toUser: number;
	content: string;
	companyName: string;

	readonly createdAt: Date;
	readonly updatedAt: Date;

	static defineSchema(sequelize: Sequelize): void {
		Feedback.init(
			{
				id: {
					type: DataTypes.INTEGER.UNSIGNED,
					autoIncrement: true,
					primaryKey: true,
				},
				fromUser: {
					type: DataTypes.INTEGER.UNSIGNED,
					allowNull: false,
					onDelete: "cascade",
				},
				toUser: {
					type: DataTypes.INTEGER.UNSIGNED,
					allowNull: false,
					onDelete: "cascade",
				},
				content: {
					type: DataTypes.TEXT,
					allowNull: false,
				},
				companyName: {
					type: DataTypes.STRING(128),
					allowNull: false,
				},
			},
			{
				tableName: "feedbacks",
				underscored: true,
				sequelize,
			}
		);
	}

	static associate(models: Models, sequelize: Sequelize): void {
		Feedback.belongsTo(models.user, { foreignKey: "fromUser", as: "sender" });
		Feedback.belongsTo(models.user, { foreignKey: "toUser", as: "receiver" });
	}
}
