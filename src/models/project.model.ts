import { DataTypes, Model, type Optional, type Sequelize } from "sequelize";
import { type Models } from "../../src/interfaces/general";

export interface ProjectAttributes {
	id: number;
	userId: number;
	image: string;
	description: string;
}

export class Project
	extends Model<ProjectAttributes, Optional<ProjectAttributes, "id">>
	implements ProjectAttributes
{
	id: number;
	userId: number;
	image: string;
	description: string;

	static defineSchema(sequelize: Sequelize): void {
		Project.init(
			{
				id: {
					type: DataTypes.INTEGER.UNSIGNED,
					primaryKey: true,
					autoIncrement: true,
				},
				userId: {
					type: DataTypes.INTEGER.UNSIGNED,
					allowNull: false,
				},
				image: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				description: {
					type: DataTypes.TEXT,
					allowNull: false,
				},
			},
			{
				tableName: "projects",
				underscored: true,
				sequelize,
			}
		);
	}

	static associate(models: Models, sequelize: Sequelize): void {
		Project.belongsTo(models.user, { foreignKey: "userId" });
	}
}
