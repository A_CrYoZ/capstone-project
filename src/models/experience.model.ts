import { DataTypes, Model, type Optional, type Sequelize } from "sequelize";
import { type Models } from "../../src/interfaces/general";

export interface ExperienceAttributes {
	id: number;
	userId: number;
	companyName: string;
	role: string;
	startDate: Date;
	endDate: Date | null;
	description: string;
}

export class Experience
	extends Model<ExperienceAttributes, Optional<ExperienceAttributes, "id">>
	implements ExperienceAttributes
{
	id: number;
	userId: number;
	companyName: string;
	role: string;
	startDate: Date;
	endDate: Date | null;
	description: string;

	static defineSchema(sequelize: Sequelize): void {
		Experience.init(
			{
				id: {
					type: DataTypes.INTEGER.UNSIGNED,
					autoIncrement: true,
					primaryKey: true,
				},
				userId: {
					type: DataTypes.INTEGER.UNSIGNED,
					allowNull: false,
					references: {
						model: "users",
						key: "id",
					},
				},
				companyName: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				role: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				startDate: {
					type: DataTypes.DATE,
					allowNull: false,
				},
				endDate: {
					type: DataTypes.DATE,
					allowNull: true,
				},
				description: {
					type: DataTypes.TEXT,
					allowNull: false,
				},
			},
			{
				tableName: "experiences",
				underscored: true,
				sequelize,
			}
		);
	}

	static associate(models: Models, sequelize: Sequelize): void {
		this.belongsTo(models.user, { foreignKey: "userId" });
	}
}
