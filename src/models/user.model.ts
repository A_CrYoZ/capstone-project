import { DataTypes, Model, type Optional, type Sequelize } from "sequelize";
import { type UserRole, type Models } from "../interfaces/general";

export interface UserAttributes {
	id: number;
	firstName: string;
	lastName: string;
	image: string;
	title: string;
	summary: string;
	role: UserRole;
	email: string;
	password: string;
}

export class User
	extends Model<UserAttributes, Optional<UserAttributes, "id">>
	implements UserAttributes
{
	id: number;
	firstName: string;
	lastName: string;
	image: string;
	title: string;
	summary: string;
	role: UserRole;
	email: string;
	password: string;

	readonly createdAt: Date;
	readonly updatedAt: Date;

	static defineSchema(sequelize: Sequelize): void {
		User.init(
			{
				id: {
					type: DataTypes.INTEGER.UNSIGNED,
					autoIncrement: true,
					primaryKey: true,
				},
				firstName: {
					field: "first_name",
					type: DataTypes.STRING(128),
					allowNull: false,
				},
				lastName: {
					field: "last_name",
					type: DataTypes.STRING(128),
					allowNull: false,
				},
				image: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				title: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				summary: {
					type: DataTypes.STRING(256),
					allowNull: false,
				},
				role: {
					type: DataTypes.STRING(50),
					allowNull: false,
				},
				email: {
					type: DataTypes.STRING,
					allowNull: false,
				},
				password: {
					type: DataTypes.STRING,
					allowNull: false,
				},
			},
			{
				tableName: "users",
				underscored: true,
				sequelize,
			}
		);
	}

	static associate(models: Models, sequelize: Sequelize): void {
		// Feedback relations
		User.hasMany(models.feedback, {
			foreignKey: "fromUser",
			as: "sentFeedbacks",
		});
		User.hasMany(models.feedback, {
			foreignKey: "toUser",
			as: "feedbacks",
		});

		// Experience relations
		User.hasMany(models.experience, {
			foreignKey: "userId",
			as: "experiences",
		});

		// Project relations
		User.hasMany(models.project, {
			foreignKey: "userId",
			as: "projects",
		});
	}
}
