import express, {
	type Router,
	type Response,
	type NextFunction,
} from "express";
import {
	UserRole,
	type Context,
	type RouterFactory,
	ErrorMsgs,
} from "../interfaces/general";
import {
	authorizeProject,
	authorizeUser,
} from "../middleware/authorize.users.middleware";
import { uploadImage } from "../utils/multer.storage.initializer";
import { projectsParamsValidators } from "../middleware/sanitizers-validators/projects.params.validator.middleware";
import errorValidator from "../middleware/express.errors.validator";
import { type ExtendedRequest } from "../interfaces/express";
import createHttpError from "http-errors";
import { param, query } from "express-validator";
import { deleteImage } from "../utils/image.deletion";

export const makeProjectsRouter: RouterFactory = (context: Context): Router => {
	const router = express.Router();

	// Destruct authService from context obj.
	const {
		services: { authService, projectsService, cacheService },
	} = context;

	router.post(
		"/",
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin, UserRole.User]),
		uploadImage.single("image"),
		projectsParamsValidators,
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			if (req.user === undefined) {
				next(createHttpError(500, ErrorMsgs.INTERNAL_SERVER_ERROR));
				return;
			}

			try {
				const { userId, description } = req.body;

				const fPath = req.file?.path;

				if (!fPath) {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const project = await projectsService.createProject(
					userId,
					fPath,
					description,
					cacheService
				);

				res.status(201).json({
					...project.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.put(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeProject(projectsService),
		uploadImage.single("image"),
		projectsParamsValidators,
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const id = req.params.id;

			if (typeof id !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const { userId, description } = req.body;

			const fPath = req.file?.path;

			if (!fPath) {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			try {
				const project = await projectsService.updateProject(
					id,
					userId,
					fPath,
					description,
					cacheService
				);

				res.status(200).json({
					...project?.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.delete(
		"/:id",
		authService.passport.authenticate("jwt", { session: false }),
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authorizeProject(projectsService),
		async (
			req: ExtendedRequest & { projectImg: string },
			res: Response,
			next: NextFunction
		) => {
			const { id } = req.params;

			if (typeof id !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const { projectImg } = req;

			if (projectImg) await deleteImage(projectImg);

			const result = await projectsService.deleteProjectById(id, cacheService);
			result
				? res.status(204).send()
				: res.status(404).json({ message: ErrorMsgs.NOT_FOUND });
		}
	);

	router.get(
		"/",
		query("pageSize").escape().trim().isNumeric().toInt(10),
		query("page").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin]),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { pageSize, page } = req.query;

			if (typeof page !== "number" || typeof pageSize !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const offset = page * pageSize;
			const limit = pageSize as number;

			try {
				const projects = await projectsService.listAllProjects(limit, offset);

				res
					.setHeader("X-total-count", projects.count)
					.status(200)
					.json(projects.rows);
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		async (
			req: Omit<ExtendedRequest, "user">,
			res: Response,
			next: NextFunction
		) => {
			try {
				const id = req.params.id;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const project = await projectsService.findProjectById(id);
				project
					? res.status(200).json({
							...project.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	return router;
};
