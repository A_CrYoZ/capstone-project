// #region Imports
import express, {
	type Router,
	type Response,
	type NextFunction,
} from "express";
import {
	type Context,
	type RouterFactory,
	ErrorMsgs,
} from "../interfaces/general";
import { registrationParamsValidators } from "../middleware/sanitizers-validators/registration.params.validator.middleware";
import errorValidator from "../middleware/express.errors.validator";
import path from "path";
import {
	authorizeUser,
	authorizeUserDeletion,
	authorizeUserUpdate,
} from "../middleware/authorize.users.middleware";
import { UserRole } from "../interfaces/general";
import { type ExtendedRequest } from "../interfaces/express";
import createHttpError from "http-errors";
import { check, param, query } from "express-validator";
import { deleteImage } from "../utils/image.deletion";
import { uploadImage } from "../utils/multer.storage.initializer";
// #endregion

export const makeUsersRouter: RouterFactory = (context: Context): Router => {
	const router = express.Router();

	// Destruct authService from context obj.
	const {
		services: { authService, userService, cacheService },
	} = context;

	router.post(
		"/",
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin]),
		uploadImage.single("avatar"),
		registrationParamsValidators,
		check("role").escape().trim().isIn([UserRole.Admin, UserRole.User]),
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			if (req.user === undefined) {
				next(createHttpError(500, ErrorMsgs.INTERNAL_SERVER_ERROR));
				return;
			}

			try {
				const { firstName, lastName, title, summary, role, email, password } =
					req.body;
				const hashedPassword = await userService.hashPassword(password);

				const filePath =
					req.file?.path ?? path.join(__dirname, "../../public/default.png");

				const user = await userService.createUser(
					firstName,
					lastName,
					filePath,
					title,
					summary,
					email,
					hashedPassword,
					role
				);

				res.status(201).json({
					...user.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
					password: undefined,
					image: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.put(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUserUpdate,
		uploadImage.single("avatar"),
		registrationParamsValidators,
		check("role").escape().trim().isIn([UserRole.Admin, UserRole.User]),
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			if (req.user === undefined) {
				next(createHttpError(500, ErrorMsgs.INTERNAL_SERVER_ERROR));
				return;
			}

			const id = req.params.id;

			if (typeof id !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const { firstName, lastName, title, summary, role, email, password } =
				req.body;

			try {
				const hashedPassword = await userService.hashPassword(password);

				const filePath = req.file?.path;

				const user = await userService.updateUser(
					id,
					firstName,
					lastName,
					filePath,
					title,
					summary,
					email,
					hashedPassword,
					role as UserRole,
					cacheService
				);

				res.status(200).json({
					...user?.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
					image: undefined,
					password: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.delete(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUserDeletion,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			try {
				const id = parseInt(req.params.id);

				const user = await userService.findById(id);

				if (!user) {
					res.status(404).json({ message: ErrorMsgs.NOT_FOUND });
					return;
				}

				await deleteImage(user?.image);

				const result = await userService.deleteUserById(id, cacheService);

				result
					? res.status(204).send()
					: res.status(404).json({ message: ErrorMsgs.NOT_FOUND });
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/",
		query("pageSize").escape().trim().isNumeric().toInt(10),
		query("page").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin]),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { pageSize, page } = req.query;

			if (typeof page !== "number" || typeof pageSize !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}
			const offset = page * pageSize;
			const limit = pageSize as number;

			try {
				const users = await userService.listAllUsers(limit, offset);

				res
					.setHeader("X-total-count", users.count)
					.status(200)
					.json(users.rows);
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		async (
			req: Omit<ExtendedRequest, "user">,
			res: Response,
			next: NextFunction
		) => {
			try {
				const id = req.params.id;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const user = await userService.findById(id);
				user
					? res.status(200).json({
							...user.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
							image: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/:userId/cv",
		param("userId").escape().trim().isNumeric().toInt(10),
		errorValidator,
		async (
			req: Omit<ExtendedRequest, "user">,
			res: Response,
			next: NextFunction
		) => {
			const { userId } = req.params;

			if (typeof userId !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			try {
				const cv = await userService.getUserCV(userId, cacheService);

				if (!cv) {
					next(createHttpError(404, ErrorMsgs.NOT_FOUND));
					return;
				}

				res.status(200).json({ ...cv });
			} catch (error) {
				next(error);
			}
		}
	);

	return router;
};
