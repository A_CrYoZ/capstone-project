// #region Add imports
import express, {
	type Router,
	type Response,
	type NextFunction,
} from "express";
import {
	type Context,
	type RouterFactory,
	UserRole,
	ErrorMsgs,
} from "../interfaces/general";
import {
	authorizeFeedback,
	authorizeUser,
} from "../middleware/authorize.users.middleware";
import { feedbackParamsValidators } from "../middleware/sanitizers-validators/feedbacks.params.validator.middleware";
import errorValidator from "../middleware/express.errors.validator";
import { type ExtendedRequest } from "../interfaces/express";
import { param, query } from "express-validator";
import createHttpError from "http-errors";
// #endregion

export const makeFeedbackRouter: RouterFactory = (context: Context): Router => {
	const router = express.Router();

	const {
		services: { authService, feedbackService, cacheService },
	} = context;

	router.post(
		"/",
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin, UserRole.User]),
		feedbackParamsValidators,
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { fromUser, toUser, companyName, content } = req.body;

			try {
				const feedback = await feedbackService.createFeedback(
					fromUser,
					toUser,
					companyName,
					content,
					cacheService
				);

				res.status(201).json({
					...feedback.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.put(
		"/:id",
		authService.passport.authenticate("jwt", { session: false }),
		feedbackParamsValidators,
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authorizeFeedback(feedbackService),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			try {
				const { id } = req.params;
				const { fromUser, toUser, companyName, content } = req.body;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const feedback = await feedbackService.updateFeedback(
					id,
					fromUser,
					toUser,
					companyName,
					content,
					cacheService
				);

				feedback
					? res.status(200).json({
							...feedback.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	router.delete(
		"/:id",
		authService.passport.authenticate("jwt", { session: false }),
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authorizeFeedback(feedbackService),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { id } = req.params;

			if (typeof id !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const result = await feedbackService.deleteFeedbackById(id, cacheService);
			result
				? res.status(204).send()
				: res.status(404).json({ message: ErrorMsgs.NOT_FOUND });
		}
	);

	router.get(
		"/",
		query("pageSize").escape().trim().isNumeric().toInt(10),
		query("page").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin]),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { pageSize, page } = req.query;

			if (typeof page !== "number" || typeof pageSize !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const offset = page * pageSize;
			const limit = pageSize as number;

			try {
				const feedbacks = await feedbackService.listAllFeedbacks(limit, offset);

				res
					.setHeader("X-total-count", feedbacks.count)
					.status(200)
					.json(feedbacks.rows);
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		async (
			req: Omit<ExtendedRequest, "user">,
			res: Response,
			next: NextFunction
		) => {
			try {
				const id = req.params.id;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const feedback = await feedbackService.findFeedbackById(id);
				feedback
					? res.status(200).json({
							...feedback.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	return router;
};
