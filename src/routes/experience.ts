import express, {
	type NextFunction,
	type Response,
	type Router,
} from "express";
import {
	UserRole,
	type Context,
	type RouterFactory,
	ErrorMsgs,
} from "../interfaces/general";
import {
	authorizeExperience,
	authorizeUser,
} from "../middleware/authorize.users.middleware";
import { experienceParamsValidators } from "../middleware/sanitizers-validators/experiences.params.validator.middleware";
import errorValidator from "../middleware/express.errors.validator";
import { type ExtendedRequest } from "../interfaces/express";
import { param, query } from "express-validator";
import createHttpError from "http-errors";

export const makeExperienceRouter: RouterFactory = (
	context: Context
): Router => {
	const router = express.Router();

	// Destruct authService from context obj.
	const {
		services: { authService, experienceService, cacheService },
	} = context;

	router.post(
		"/",
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin, UserRole.User]),
		experienceParamsValidators,
		errorValidator,
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { userId, companyName, role, startDate, endDate, description } =
				req.body;

			try {
				const exp = await experienceService.createExperience(
					userId,
					companyName,
					role,
					startDate,
					endDate,
					description,
					cacheService
				);

				res.status(201).json({
					...exp.toJSON(),
					createdAt: undefined,
					updatedAt: undefined,
				});
			} catch (error) {
				next(error);
			}
		}
	);

	router.put(
		"/:id",
		authService.passport.authenticate("jwt", { session: false }),
		experienceParamsValidators,
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authorizeExperience(experienceService),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			try {
				const { id } = req.params;
				const { userId, companyName, role, startDate, endDate, description } =
					req.body;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const exp = await experienceService.updateExperience(
					id,
					userId,
					companyName,
					role,
					startDate,
					endDate,
					description,
					cacheService
				);

				exp
					? res.status(200).json({
							...exp.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	router.delete(
		"/:id",
		authService.passport.authenticate("jwt", { session: false }),
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authorizeExperience(experienceService),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { id } = req.params;

			if (typeof id !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const result = await experienceService.deleteExperienceById(
				id,
				cacheService
			);
			result
				? res.status(204).send()
				: res.status(404).json({ message: ErrorMsgs.NOT_FOUND });
		}
	);

	router.get(
		"/",
		query("pageSize").escape().trim().isNumeric().toInt(10),
		query("page").escape().trim().isNumeric().toInt(10),
		errorValidator,
		authService.passport.authenticate("jwt", { session: false }),
		authorizeUser([UserRole.Admin]),
		async (req: ExtendedRequest, res: Response, next: NextFunction) => {
			const { pageSize, page } = req.query;

			if (typeof page !== "number" || typeof pageSize !== "number") {
				next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
				return;
			}

			const offset = page * pageSize;
			const limit = pageSize as number;

			try {
				const exps = await experienceService.listAllExperiences(limit, offset);

				res.setHeader("X-total-count", exps.count).status(200).json(exps.rows);
			} catch (error) {
				next(error);
			}
		}
	);

	router.get(
		"/:id",
		param("id").escape().trim().isNumeric().toInt(10),
		errorValidator,
		async (
			req: Omit<ExtendedRequest, "user">,
			res: Response,
			next: NextFunction
		) => {
			try {
				const id = req.params.id;

				if (typeof id !== "number") {
					next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
					return;
				}

				const exp = await experienceService.findExperienceById(id);
				exp
					? res.status(200).json({
							...exp.toJSON(),
							createdAt: undefined,
							updatedAt: undefined,
					  })
					: next(createHttpError(404, ErrorMsgs.NOT_FOUND));
			} catch (error) {
				next(error);
			}
		}
	);

	return router;
};
