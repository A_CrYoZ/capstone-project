// #region Imports
import {
  type Context,
  type RouterFactory,
  ErrorMsgs,
} from "../interfaces/general";
import express, {
  type NextFunction,
  type Response,
  type Router,
} from "express";
import { type ExtendedRequest } from "../interfaces/express";
import { check } from "express-validator";
import multer from "multer";
import errorValidator from "../middleware/express.errors.validator";
import createHttpError from "http-errors";
import jwt from "jsonwebtoken";
import { logger } from "../libs/logger";
import path from "path";
import { type User } from "../models/user.model";
import { config } from "../config";
import { registrationParamsValidators } from "../middleware/sanitizers-validators/registration.params.validator.middleware";
// #endregion

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../../public"));
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname),
    );
  },
});
const upload = multer({ storage });

export const makeAuthRouter: RouterFactory = (context: Context): Router => {
  const router = express.Router();

  // Destruct authService from context obj.
  const {
    services: { authService, userService },
  } = context;

  router.post(
    "/register",
    upload.single("avatar"),
    registrationParamsValidators,
    errorValidator,
    authService.passport.authenticate("register", { session: false }),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      if (req.user === undefined) {
        next(createHttpError(500, ErrorMsgs.INTERNAL_SERVER_ERROR));
        return;
      }

      const { firstName, lastName, title, summary } = req.body;
      const { email, password } = req.user;

      if (!req.file) {
        logger.info({
          requestId: req.id,
          message: "Image is not provided",
        });
        next(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
        return;
      }

      try {
        const user = await userService.createUser(
          firstName,
          lastName,
          req.file.path,
          title,
          summary,
          email,
          password as string,
        );

        res.status(201).json({
          ...user.toJSON(),
          createdAt: undefined,
          updatedAt: undefined,
          password: undefined,
        });
      } catch (error) {
        next(error);
      }
    },
  );
  router.post(
    "/login",
    check("email")
      .escape()
      .trim()
      .isEmail()
      .isLength({ min: 6 })
      .withMessage("Email length cannot be less than 6 symbols"),
    check("password").escape().trim(),
    errorValidator,
    (req: ExtendedRequest, res: Response, next: NextFunction) => {
      authService.passport.authenticate(
        "login",
        (err: Error, user: User, info: unknown) => {
          if (err) {
            next(err);
            return;
          }

          req.login(user, { session: false }, (error) => {
            if (error) {
              next(error);
              return;
            }

            // Remove password from object
            const body = {
              ...user,
              password: undefined,
              iat: Date.now(),
            };

            const token = jwt.sign({ user: body }, config.auth.secret, {
              expiresIn: "1d",
            });

            res.status(200).json({ token });
          });
        },
      )(req, res, next);
    },
  );
  return router;
};
