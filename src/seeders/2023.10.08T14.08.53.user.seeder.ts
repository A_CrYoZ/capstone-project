import type { MigrationFn } from "umzug";
import { type Sequelize } from "sequelize";

import { UserService } from "../services/user.service";
import { type UserAttributes } from "../models/user.model";
import path from "path";
import { UserRole } from "../interfaces/general";

type migrationUserAttributes = Omit<
	UserAttributes,
	"lastName" | "firstName"
> & {
	first_name: string;
	last_name: string;
	created_at: string;
	updated_at: string;
};

export const up: MigrationFn<Sequelize> = async ({ context }) => {
	const q = context.getQueryInterface();

	const now = new Date();
	const isoString = now.toISOString();
	const mysqlFormattedTime = isoString
		.replace("T", " ")
		.replace("Z", "")
		.slice(0, 19);

	const user: migrationUserAttributes = {
		email: "vtsoyer@gmail.com",
		first_name: "Vladyslav",
		last_name: "Tarapata",
		image: path.join(__dirname, "../../public/default.png"),
		password: await UserService.hashPassword("root"),
		role: UserRole.Admin,
		summary: "Node.JS Developer",
		title: "Software Engineer",
		created_at: mysqlFormattedTime,
		updated_at: mysqlFormattedTime,
		id: 1,
	};
	console.log(user);

	await q.bulkInsert("users", [user]);
};
export const down: MigrationFn<Sequelize> = async ({ context }) => {
	const q = context.getQueryInterface();

	await q.bulkDelete("users", { id: 1 });
};
