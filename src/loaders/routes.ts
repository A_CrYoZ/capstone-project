import { type Router } from "express";
import { type Loader, type Context } from "../interfaces/general";
import { makeAuthRouter } from "../routes/auth";
import { makeUsersRouter } from "../routes/users";
import notFoundMiddleware from "../middleware/not.found.middleware";
import { makeExperienceRouter } from "../routes/experience";
import { makeFeedbackRouter } from "../routes/feedback";
import { makeProjectsRouter } from "../routes/projects";

export const loadRoutes: Loader = (app: Router, context: Context): void => {
  app.use("/api/auth", makeAuthRouter(context));
  app.use("/api/users", makeUsersRouter(context));
  app.use("/api/experience", makeExperienceRouter(context));
  app.use("/api/feedback", makeFeedbackRouter(context));
  app.use("/api/projects", makeProjectsRouter(context));
  app.use(notFoundMiddleware);
};
