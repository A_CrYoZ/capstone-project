import { type Loader } from "../interfaces/general";

export const loadPassport: Loader = (app, context) => {
  app.use(context.services.authService.passportHandler);
};
