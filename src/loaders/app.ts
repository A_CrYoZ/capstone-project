import { loadErrorHandler, loadMiddlewares } from "./middlewares";
import { loadRoutes } from "./routes";
import express, { type Express } from "express";
import { loadContext } from "./context";
import { loadModels } from "./models";
import { loadSequelize } from "./sequelize";
import { config } from "../config";
import { loadPassport } from "./passport";
import { logger } from "../libs/logger";

export const loadApp = async (): Promise<Express> => {
	const app = express();
	const sequelize = loadSequelize(config);

	const context = await loadContext();

	// Check connection to database
	await sequelize.authenticate();
	logger.info("Database connected");

	// Connect to Redis
	await context.services.cacheService.connect();
	logger.info("Redis connected");

	loadModels(sequelize);

	loadPassport(app, context);
	loadMiddlewares(app, context);
	loadRoutes(app, context);

	loadErrorHandler(app, context);

	return app;
};
