import { UserService } from "../services/user.service";
import { type Context } from "../interfaces/general";
import { AuthService } from "../services/auth.service";
import { ExperienceService } from "../services/experience.service";
import { FeedbackService } from "../services/feedback.service";
import { ProjectsService } from "../services/projects.service";
import { CacheService } from "../services/cache.service";

export const loadContext = async (): Promise<Context> => {
	return {
		services: {
			authService: new AuthService(),
			userService: UserService,
			experienceService: ExperienceService,
			feedbackService: FeedbackService,
			projectsService: ProjectsService,
			cacheService: new CacheService(),
		},
	};
};
