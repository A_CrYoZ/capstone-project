import { Sequelize } from "sequelize";
import { type Config } from "../config";
import { logger } from "../libs/logger";

export const loadSequelize = (config: Config): Sequelize => {
  return new Sequelize({
    ...config.db,
    logging: (msg) => {
      logger.info(msg);
    },
  });
};
