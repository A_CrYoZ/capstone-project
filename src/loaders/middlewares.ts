import { type Loader, type Context } from "../interfaces/general";
import express, { type Express } from "express";
import requestID from "express-request-id";
import { clsProxifyExpressMiddleware } from "cls-proxify/integration/express";
import errorHandlerMiddleware from "../middleware/error.handler.middleware";
import requestInfo from "../middleware/request.details.middleware";
import bodyParser from "body-parser";
import { type ExtendedRequest } from "../interfaces/express";
import { baseLogger } from "../libs/logger";

export const loadMiddlewares: Loader = (app: Express, context: Context) => {
	// Disable standard header which indicates server built with express
	app.disable("x-powered-by");
	// Add requestId to the headers
	app.use(requestID());
	app.use(express.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(
		clsProxifyExpressMiddleware((req: ExtendedRequest) => {
			const requestID = req.id;
			return baseLogger.child({ requestID });
		})
	);
	app.use(requestInfo);
};

export const loadErrorHandler: Loader = (app: Express, context: Context) => {
	app.use(errorHandlerMiddleware);
};
