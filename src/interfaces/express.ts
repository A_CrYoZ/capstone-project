import { type Request } from "express";
import { type UserAttributes } from "../models/user.model";

export type ExtendedRequest = Request & {
  id: string;
  user?: Omit<UserAttributes, "password"> & {
    createdAt: Date;
    updatedAt: Date;
    password?: string;
  };
};
