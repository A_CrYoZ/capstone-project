import type express from "express";
import { type AuthService } from "../services/auth.service";
import { type User } from "../models/user.model";
import { type Experience } from "../models/experience.model";
import { type Feedback } from "../models/feedback.model";
import { type Project } from "../models/project.model";
import { type UserService } from "../services/user.service";
import { type ExperienceService } from "../services/experience.service";
import { type FeedbackService } from "../services/feedback.service";
import { type ProjectsService } from "../services/projects.service";
import { type CacheService } from "../services/cache.service";

export interface Context {
	services: {
		authService: AuthService;
		userService: typeof UserService;
		experienceService: typeof ExperienceService;
		feedbackService: typeof FeedbackService;
		projectsService: typeof ProjectsService;
		cacheService: CacheService;
	};
}

export enum ErrorMsgs {
	NOT_UNIQUE_EMAIL = "email should be a unique field.",
	VALIDATION_ERROR = "Validation failed",
	INTERNAL_SERVER_ERROR = "Internal Server Error",
	NOT_FOUND = "Not Found",
	UNAUTHORIZED = "Unauthorized",
}

export type RouterFactory = (context: Context) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export interface Models {
	user: typeof User;
	experience: typeof Experience;
	feedback: typeof Feedback;
	project: typeof Project;
}

export enum UserRole {
	Admin = "Admin",
	User = "User",
}
