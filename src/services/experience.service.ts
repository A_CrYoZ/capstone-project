import createHttpError from "http-errors";
import { Experience } from "../models/experience.model";
import { ErrorMsgs } from "../interfaces/general";
import { type CacheService } from "./cache.service";

export class ExperienceService {
	static async createExperience(
		userId: number,
		companyName: string,
		role: string,
		startDate: Date,
		endDate: Date,
		description: string,
		cacheService: CacheService
	): Promise<Experience> {
		try {
			const exp = await Experience.create({
				userId,
				companyName,
				role,
				startDate,
				endDate,
				description,
			});

			await cacheService.deletePair(userId.toString());

			return exp;
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async updateExperience(
		id: number,
		userId: number,
		companyName: string,
		role: string,
		startDate: Date,
		endDate: Date,
		description: string,
		cacheService: CacheService
	): Promise<Experience | null> {
		try {
			const [rowCount] = await Experience.update(
				{
					userId,
					companyName,
					role,
					startDate,
					endDate,
					description,
				},
				{
					where: {
						id,
					},
				}
			);

			if (rowCount > 0) {
				await cacheService.deletePair(userId.toString());
				return await ExperienceService.findExperienceById(id);
			}

			throw createHttpError(404, ErrorMsgs.NOT_FOUND);
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async deleteExperienceById(
		id: number,
		cacheService: CacheService
	): Promise<boolean> {
		const result = await ExperienceService.findExperienceById(id);

		if (!result) throw createHttpError(404, ErrorMsgs.NOT_FOUND);

		await cacheService.deletePair(result.userId.toString());

		return (await Experience.destroy({ where: { id } })) > 0;
	}

	static async listAllExperiences(
		limit: number,
		offset: number
	): Promise<{ rows: Experience[]; count: number }> {
		return await Experience.findAndCountAll({
			limit,
			offset,
			order: [["id", "ASC"]],
			attributes: {
				exclude: ["createdAt", "updatedAt"],
			},
		});
	}

	static async findExperienceById(id: number): Promise<Experience | null> {
		return await Experience.findByPk(id);
	}
}
