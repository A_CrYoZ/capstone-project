import createHttpError from "http-errors";
import { Feedback } from "../models/feedback.model";
import { ErrorMsgs } from "../interfaces/general";
import { type CacheService } from "./cache.service";

export class FeedbackService {
	static async createFeedback(
		fromUser: number,
		toUser: number,
		companyName: string,
		content: string,
		cacheService: CacheService
	): Promise<Feedback> {
		try {
			const feedback = await Feedback.create({
				fromUser,
				toUser,
				companyName,
				content,
			});

			await cacheService.deletePair(toUser.toString());

			return feedback;
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async updateFeedback(
		id: number,
		fromUser: number,
		toUser: number,
		companyName: string,
		content: string,
		cacheService: CacheService
	): Promise<Feedback | null> {
		try {
			const [rowCount] = await Feedback.update(
				{
					fromUser,
					toUser,
					companyName,
					content,
				},
				{
					where: {
						id,
					},
				}
			);

			if (rowCount > 0) {
				await cacheService.deletePair(toUser.toString());
				return await FeedbackService.findFeedbackById(id);
			}

			throw createHttpError(404, ErrorMsgs.NOT_FOUND);
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async deleteFeedbackById(
		id: number,
		cacheService: CacheService
	): Promise<boolean> {
		const result = await FeedbackService.findFeedbackById(id);

		if (!result) throw createHttpError(404, ErrorMsgs.NOT_FOUND);

		await cacheService.deletePair(result.toUser.toString());

		return (await Feedback.destroy({ where: { id } })) > 0;
	}

	static async listAllFeedbacks(
		limit: number,
		offset: number
	): Promise<{ rows: Feedback[]; count: number }> {
		return await Feedback.findAndCountAll({
			limit,
			offset,
			order: [["id", "ASC"]],
			attributes: {
				exclude: ["createdAt", "updatedAt"],
			},
		});
	}

	static async findFeedbackById(id: number): Promise<Feedback | null> {
		return await Feedback.findByPk(id);
	}
}
