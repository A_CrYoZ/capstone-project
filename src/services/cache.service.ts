import { createClient } from "redis";
import { config } from "../config";

export class CacheService {
	private readonly client;

	constructor() {
		this.client = createClient({
			socket: {
				host: config.redis.host,
				port: config.redis.port,
			},
		});
	}

	async connect(): Promise<void> {
		await this.client.connect();
	}

	async setPair(key: string, value: object): Promise<string | null> {
		return await this.client.set(key, JSON.stringify(value));
	}

	async getPair(key: string): Promise<object | null> {
		const res = await this.client.get(key);
		return res ? JSON.parse(res) : null;
	}

	async deletePair(key: string): Promise<boolean> {
		return (await this.client.del(key)) > 0;
	}
}
