import { User, type UserAttributes } from "../models/user.model";
import createHttpError from "http-errors";
import bcrypt from "bcrypt";
import { ErrorMsgs, UserRole } from "../interfaces/general";
import { Experience } from "../models/experience.model";
import { Project } from "../models/project.model";
import { Feedback } from "../models/feedback.model";
import { type CacheService } from "./cache.service";

import { type ProjectAttributes } from "../models/project.model";
import { type FeedbackAttributes } from "../models/feedback.model";
import { type ExperienceAttributes } from "../models/experience.model";

export interface CV extends UserAttributes {
	projects: Partial<ProjectAttributes>;
	feedbacks: Partial<FeedbackAttributes>;
	experiences: Partial<ExperienceAttributes>;
}

export class UserService {
	static async createUser(
		firstName: string,
		lastName: string,
		image: string,
		title: string,
		summary: string,
		email: string,
		password: string,
		role?: UserRole
	): Promise<User> {
		const user = await User.findOne({
			where: { email },
		});

		if (user !== null) throw createHttpError(400, "User already exists");

		if (!role) role = UserRole.User;

		return await User.create({
			firstName,
			lastName,
			image,
			title,
			summary,
			email,
			password,
			role,
		});
	}

	static async updateUser(
		id: number,
		firstName: string,
		lastName: string,
		image: string | undefined,
		title: string,
		summary: string,
		email: string,
		password: string,
		role: UserRole,
		cacheService: CacheService
	): Promise<User | null> {
		try {
			const [rowCount] = await User.update(
				{
					firstName,
					lastName,
					title,
					summary,
					email,
					password,
					role,
					image,
				},
				{
					where: {
						id,
					},
				}
			);

			if (rowCount > 0) {
				await cacheService.deletePair(id.toString());
				return await UserService.findById(id);
			}

			throw createHttpError(404, ErrorMsgs.NOT_FOUND);
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async deleteUserById(
		id: number,
		cacheService: CacheService
	): Promise<boolean> {
		const result = (await User.destroy({ where: { id } })) > 0;
		if (result) await cacheService.deletePair(id.toString());
		return result;
	}

	static async findByEmail(email: string): Promise<User | null> {
		return await User.findOne({ where: { email } });
	}

	static async findById(id: number): Promise<User | null> {
		return await User.findByPk(id);
	}

	static async listAllUsers(
		limit: number,
		offset: number
	): Promise<{ rows: User[]; count: number }> {
		return await User.findAndCountAll({
			limit,
			offset,
			order: [["id", "ASC"]],
			attributes: {
				exclude: ["password", "image", "createdAt", "updatedAt"],
			},
		});
	}

	static async getUserCV(
		id: number,
		cacheService: CacheService
	): Promise<CV | null> {
		let userCv: CV | null = (await cacheService.getPair(id.toString())) as CV;

		if (!userCv) {
			const dbEntry = await User.findByPk(id, {
				include: [
					{
						model: Experience,
						attributes: {
							exclude: ["id", "createdAt", "updatedAt"],
						},
						as: "experiences",
					},
					{
						model: Project,
						attributes: {
							exclude: ["createdAt", "updatedAt"],
						},
						as: "projects",
					},
					{
						model: Feedback,
						attributes: {
							exclude: ["createdAt", "updatedAt"],
						},
						as: "feedbacks",
					},
				],
				attributes: {
					exclude: ["createdAt", "updatedAt", "password", "role"],
				},
			});

			userCv = dbEntry?.toJSON() as CV | null;

			// Cache CV
			if (userCv) await cacheService.setPair(id.toString(), userCv);
		}

		return userCv;
	}

	static async hashPassword(password: string): Promise<string> {
		return await bcrypt.hash(password, 10);
	}

	static async comparePasswords(
		password: string,
		originalPassword: string
	): Promise<boolean> {
		return await bcrypt.compare(password, originalPassword);
	}
}
