import createHttpError from "http-errors";
import { ErrorMsgs } from "../interfaces/general";
import { Project } from "../models/project.model";
import { type CacheService } from "./cache.service";

export class ProjectsService {
	static async createProject(
		userId: number,
		image: string,
		description: string,
		cacheService: CacheService
	): Promise<Project> {
		try {
			const project = await Project.create({
				userId,
				image,
				description,
			});

			await cacheService.deletePair(userId.toString());

			return project;
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async updateProject(
		id: number,
		userId: number,
		image: string,
		description: string,
		cacheService: CacheService
	): Promise<Project | null> {
		try {
			const [rowCount] = await Project.update(
				{
					userId,
					image,
					description,
				},
				{
					where: {
						id,
					},
				}
			);

			if (rowCount > 0) {
				await cacheService.deletePair(userId.toString());
				return await ProjectsService.findProjectById(id);
			}

			throw createHttpError(404, ErrorMsgs.NOT_FOUND);
		} catch (error) {
			if (error.parent?.errno === 1452) {
				throw createHttpError(400, ErrorMsgs.VALIDATION_ERROR);
			}
			throw error;
		}
	}

	static async deleteProjectById(
		id: number,
		cacheService: CacheService
	): Promise<boolean> {
		const result = await ProjectsService.findProjectById(id);
		if (!result) throw createHttpError(404, ErrorMsgs.NOT_FOUND);

		await cacheService.deletePair(result.userId.toString());

		return (await Project.destroy({ where: { id } })) > 0;
	}

	static async listAllProjects(
		limit: number,
		offset: number
	): Promise<{ rows: Project[]; count: number }> {
		return await Project.findAndCountAll({
			limit,
			offset,
			order: [["id", "ASC"]],
			attributes: {
				exclude: ["createdAt", "updatedAt"],
			},
		});
	}

	static async findProjectById(id: number): Promise<Project | null> {
		return await Project.findByPk(id);
	}
}
