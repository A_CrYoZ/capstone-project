import passport, { type PassportStatic } from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import { type Handler } from "express";
import bcrypt from "bcrypt";
import createHttpError from "http-errors";

import { UserService } from "./user.service";
import { config } from "../config";
import { ErrorMsgs } from "../interfaces/general";

export class AuthService {
	passportHandler: Handler;
	passport: PassportStatic;

	constructor() {
		this.passportHandler = AuthService.initPassport();
		this.passport = passport;
	}

	private static initPassport(): Handler {
		passport.use(
			"register",
			new LocalStrategy(
				{ usernameField: "email", passwordField: "password" },
				(email, password, done) => {
					// Find user in database and if exists: throw an error, otherwise - return user with hashed password
					UserService.findByEmail(email).then(
						async (user) => {
							if (user !== null) {
								done(createHttpError(400, ErrorMsgs.NOT_UNIQUE_EMAIL));
								return;
							}

							const hashedPassword = await bcrypt.hash(password, 10);
							done(null, { email, password: hashedPassword });
						},
						(error) => {
							done(error);
						}
					);
				}
			)
		);

		passport.use(
			"login",
			new LocalStrategy(
				{ usernameField: "email", passwordField: "password" },
				(email, password, done) => {
					UserService.findByEmail(email).then(
						async (user) => {
							if (user === null) {
								done(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
								return;
							}

							const validate = await UserService.comparePasswords(
								password,
								user.password
							);

							validate
								? done(null, { ...user.toJSON(), password: undefined })
								: done(createHttpError(400, ErrorMsgs.VALIDATION_ERROR));
						},
						(error) => {
							done(error);
						}
					);
				}
			)
		);

		passport.use(
			new JWTStrategy(
				{
					secretOrKey: config.auth.secret,
					jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
				},
				(token, done) => {
					try {
						done(null, token.user);
					} catch (err) {
						done(err);
					}
				}
			)
		);

		return passport.initialize();
	}
}
