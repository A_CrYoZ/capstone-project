export const setPairMock = jest.fn();
export const getPairMock = jest.fn();
export const deletePairMock = jest.fn();

const mock = jest.fn().mockImplementation(() => {
	return {
		setPair: setPairMock,
		getPair: getPairMock,
		deletePair: deletePairMock,
	};
});

export default mock;
