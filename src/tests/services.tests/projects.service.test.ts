import { ProjectsService } from "../../services/projects.service";
import { Project } from "../../models/project.model";
import { loadModels } from "../../loaders/models";
import mockCacheService, {
	deletePairMock,
	getPairMock,
	setPairMock,
} from "../__mocks__/cache.service.mock";
import { Sequelize } from "sequelize";
import { BadRequest, NotFound } from "http-errors";

jest.mock("../../services/cache.service", () => {
	return mockCacheService;
});

const sequelize = new Sequelize({
	dialect: "mysql",
	database: "your_database_name",
	username: "your_username",
	password: "your_password",
	host: "localhost",
	port: 5432,
});

loadModels(sequelize);

describe("Projects Service", () => {
	beforeEach(() => {
		// Clear all instances and calls to constructor and all methods:
		mockCacheService.mockClear();
		getPairMock.mockClear();
		deletePairMock.mockClear();
		setPairMock.mockClear();
	});

	it("should create and return Project and clear cache by userId", async () => {
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		jest.spyOn(Project, "create").mockImplementationOnce((projectOptions) => {
			return Project.build({
				...(projectOptions as any),
				id: 1,
			});
		});

		const result = await ProjectsService.createProject(
			1,
			"img",
			"descr",
			cacheService
		);

		expect(result).toBeInstanceOf(Project);
		expect(result.toJSON()).toStrictEqual({
			userId: 1,
			image: "img",
			description: "descr",
			id: 1,
		});
		expect(mockRedis).toBeCalledWith(result.userId.toString());
	});

	it("should throw 400 error if provided invalid userId on creation", async () => {
		const cacheService = new mockCacheService();

		jest.spyOn(Project, "create").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});

		expect(async () =>
			ProjectsService.createProject(1, "img", "descr", cacheService)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on creation", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Project, "create")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		expect(async () =>
			ProjectsService.createProject(1, "img", "descr", cacheService)
		).rejects.toThrowError("Something went wrong");
	});

	it("should update return updated project", async () => {
		const expectedProject = Project.build({
			description: "test",
			image: "test/path",
			userId: 1,
			id: 1,
		});

		jest.spyOn(Project, "update").mockResolvedValueOnce([1]);
		jest.spyOn(Project, "findByPk").mockResolvedValueOnce(expectedProject);
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const result = await ProjectsService.updateProject(
			1,
			1,
			"test/path",
			"test",
			cacheService
		);

		expect(result).toBeInstanceOf(Project);
		expect(result?.toJSON()).toStrictEqual(expectedProject.toJSON());
		expect(mockRedis).toBeCalledWith(expectedProject.userId.toString());
	});

	it("should throw 404 if not found on update", async () => {
		jest.spyOn(Project, "update").mockResolvedValueOnce([0]);
		const cacheService = new mockCacheService();

		expect(
			async () =>
				await ProjectsService.updateProject(
					1,
					1,
					"test/path",
					"test",
					cacheService
				)
		).rejects.toThrowError(NotFound);
	});

	it("should throw 400 error if provided invalid userId on update", async () => {
		jest.spyOn(Project, "update").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});
		const cacheService = new mockCacheService();

		expect(
			async () =>
				await ProjectsService.updateProject(
					1,
					1,
					"test/path",
					"test",
					cacheService
				)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on update", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Project, "update")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		expect(async () =>
			ProjectsService.updateProject(1, 1, "img", "descr", cacheService)
		).rejects.toThrowError("Something went wrong");
	});

	it("should delete project", async () => {
		const projectProps = {
			description: "",
			image: "",
			userId: 1,
			id: 1,
		};
		const deleteMock = jest.spyOn(Project, "destroy").mockResolvedValueOnce(1);
		jest
			.spyOn(Project, "findByPk")
			.mockResolvedValueOnce(Project.build(projectProps));

		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const res = await ProjectsService.deleteProjectById(1, cacheService);

		expect(deleteMock).toHaveBeenCalled();
		expect(res).toStrictEqual(true);
		expect(mockRedis).toBeCalledWith(projectProps.userId.toString());
	});

	it("should be false on project deletion if not found", async () => {
		jest.spyOn(Project, "findByPk").mockResolvedValueOnce(null);
		const cacheService = new mockCacheService();

		expect(
			async () => await ProjectsService.deleteProjectById(1, cacheService)
		).rejects.toThrowError(NotFound);
	});

	it("should return array of projects", async () => {
		const expectedObj = {
			rows: [
				Project.build({
					description: "1",
					image: "1",
					userId: 1,
					id: 1,
				}),
				Project.build({
					description: "2",
					image: "2",
					userId: 2,
					id: 2,
				}),
			],
			count: 2,
		};
		jest
			.spyOn(Project, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await ProjectsService.listAllProjects(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});

	it("should return empty array if users are not provided", async () => {
		const expectedObj = {
			rows: [],
			count: 0,
		};
		jest
			.spyOn(Project, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await ProjectsService.listAllProjects(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});
});
