import { UserService } from "../../services/user.service";
import { UserRole } from "../../interfaces/general";
import { User } from "../../models/user.model";
import { loadModels } from "../../loaders/models";
import mockCacheService, {
	deletePairMock,
	getPairMock,
	setPairMock,
} from "../__mocks__/cache.service.mock";
import { Sequelize } from "sequelize";
import { BadRequest, NotFound } from "http-errors";

jest.mock("../../services/cache.service", () => {
	return mockCacheService;
});

const sequelize = new Sequelize({
	dialect: "mysql",
	database: "your_database_name",
	username: "your_username",
	password: "your_password",
	host: "localhost",
	port: 5432,
});

loadModels(sequelize);

describe("User Service", () => {
	beforeEach(() => {
		// Clear all instances and calls to constructor and all methods:
		mockCacheService.mockClear();
		getPairMock.mockClear();
		deletePairMock.mockClear();
		setPairMock.mockClear();
	});

	it("should create and return Admin User", async () => {
		const mockFindOne = jest.spyOn(User, "findOne").mockResolvedValueOnce(null);
		jest.spyOn(User, "create").mockImplementationOnce((userOptions) => {
			return User.build({
				...(userOptions as any),
				id: 1,
			});
		});

		const result = await UserService.createUser(
			"test",
			"test",
			"some-path",
			"title",
			"summary",
			"testmail123@yahoo.com",
			"somePassword",
			UserRole.Admin
		);

		expect(mockFindOne).toHaveBeenCalledWith({
			where: { email: "testmail123@yahoo.com" },
		});
		expect(result).toBeInstanceOf(User);
		expect(result.toJSON()).toStrictEqual({
			firstName: "test",
			lastName: "test",
			image: "some-path",
			title: "title",
			summary: "summary",
			email: "testmail123@yahoo.com",
			password: "somePassword",
			role: UserRole.Admin,
			id: 1,
		});
	});

	it("should create and return User if role isn't provided", async () => {
		const mockFindOne = jest.spyOn(User, "findOne").mockResolvedValueOnce(null);
		jest.spyOn(User, "create").mockImplementationOnce((userOptions) => {
			return User.build({
				...(userOptions as any),
				id: 1,
			});
		});

		const result = await UserService.createUser(
			"test",
			"test",
			"some-path",
			"title",
			"summary",
			"testmail123@yahoo.com",
			"somePassword"
		);

		expect(mockFindOne).toHaveBeenCalledWith({
			where: { email: "testmail123@yahoo.com" },
		});
		expect(result).toBeInstanceOf(User);
		expect(result.toJSON()).toStrictEqual({
			firstName: "test",
			lastName: "test",
			image: "some-path",
			title: "title",
			summary: "summary",
			email: "testmail123@yahoo.com",
			password: "somePassword",
			role: UserRole.User,
			id: 1,
		});
	});

	it("should throw 404 error if user exists", async () => {
		const user = User.build({
			email: "",
			firstName: "",
			image: "",
			lastName: "",
			password: "",
			role: UserRole.Admin,
			summary: "",
			title: "",
		});
		const mockFindOne = jest.spyOn(User, "findOne").mockResolvedValueOnce(user);

		expect(mockFindOne).toHaveBeenCalledWith({
			where: { email: "testmail123@yahoo.com" },
		});
		expect(
			async () =>
				await UserService.createUser(
					"test",
					"test",
					"some-path",
					"title",
					"summary",
					"testmail123@yahoo.com",
					"somePassword",
					UserRole.User
				)
		).rejects.toThrowError(BadRequest);
	});

	it("should update return updated user", async () => {
		const expectedUser = User.build({
			email: "mail",
			firstName: "test",
			image: "someimg",
			lastName: "test",
			password: "pwd",
			role: UserRole.Admin,
			summary: "sum",
			title: "test",
			id: 1,
		});

		jest.spyOn(User, "update").mockResolvedValueOnce([1]);
		jest.spyOn(User, "findByPk").mockResolvedValueOnce(expectedUser);
		const cacheService = new mockCacheService();
		cacheService.deletePair.mockResolvedValueOnce(true);

		const result = await UserService.updateUser(
			1,
			"test",
			"test",
			"someimg",
			"test",
			"sum",
			"mail",
			"pwd",
			UserRole.Admin,
			cacheService
		);

		expect(result).toBeInstanceOf(User);
		expect(result).not.toBeNull();
		expect(result?.toJSON()).toStrictEqual(expectedUser.toJSON());
	});

	it("should throw 404 if not found on update", async () => {
		jest.spyOn(User, "update").mockResolvedValueOnce([0]);
		const cacheService = new mockCacheService();

		// const res = await
		expect(async () => {
			await UserService.updateUser(
				1,
				"test",
				"test",
				"someimg",
				"test",
				"sum",
				"mail",
				"pwd",
				UserRole.Admin,
				cacheService
			);
		}).rejects.toThrowError(NotFound);
	});

	it("should delete user", async () => {
		const deleteMock = jest.spyOn(User, "destroy").mockResolvedValue(1);
		const cacheService = new mockCacheService();
		cacheService.deletePair.mockResolvedValueOnce(true);

		const res = await UserService.deleteUserById(1, cacheService);

		expect(deleteMock).toHaveBeenCalled();
		expect(res).toStrictEqual(true);
	});

	it("should be false on user deletion if not found", async () => {
		const deleteMock = jest.spyOn(User, "destroy").mockResolvedValue(0);
		const cacheService = new mockCacheService();

		const res = await UserService.deleteUserById(1, cacheService);

		expect(deleteMock).toHaveBeenCalled();
		expect(res).toStrictEqual(false);
	});

	it("should return user if found by email", async () => {
		const user = User.build({
			email: "testMail",
			firstName: "test",
			image: "test/path",
			lastName: "test",
			password: "pwd",
			role: UserRole.User,
			summary: "test",
			title: "test",
			id: 1,
		});
		const findOneMock = jest.spyOn(User, "findOne").mockResolvedValue(user);

		const result = await UserService.findByEmail("testMail");

		expect(findOneMock).toHaveBeenCalledWith({ where: { email: "testMail" } });
		expect(result).toBeInstanceOf(User);
		expect(result?.toJSON()).toStrictEqual(user.toJSON());
	});

	it("should return null if user not found by email", async () => {
		const findOneMock = jest.spyOn(User, "findOne").mockResolvedValue(null);

		const result = await UserService.findByEmail("testMail");

		expect(findOneMock).toHaveBeenCalledWith({ where: { email: "testMail" } });
		expect(result).toBeNull();
	});

	it("should return array of users", async () => {
		const expectedObj = {
			rows: [
				User.build({
					email: "1",
					firstName: "1",
					image: "1",
					lastName: "1",
					password: "1",
					role: UserRole.Admin,
					summary: "1",
					title: "1",
					id: 1,
				}),
				User.build({
					email: "2",
					firstName: "2",
					image: "2",
					lastName: "2",
					password: "2",
					role: UserRole.User,
					summary: "2",
					title: "2",
					id: 2,
				}),
			],
			count: 2,
		};
		jest
			.spyOn(User, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await UserService.listAllUsers(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});

	it("should return empty array if users are not provided", async () => {
		const expectedObj = {
			rows: [],
			count: 0,
		};
		jest
			.spyOn(User, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await UserService.listAllUsers(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});

	it("should return userCV if user exists", async () => {
		const expectedCV = {
			id: 1,
			firstName: "test",
			lastName: "test",
			image: "test",
			title: "test",
			summary: "test",
			email: "test",
			// @ts-ignore
			experiences: [
				{
					userId: 1,
					companyName: "test",
					role: "Software Engineer",
					startDate: "2019-02-01T00:00:00.000Z",
					endDate: "2024-01-01T00:00:00.000Z",
					description: "Node.js developer",
				},
			],
			projects: [
				{
					id: 2,
					userId: 1,
					image: "test",
					description: "test",
				},
			],
			feedbacks: [
				{
					id: 6,
					fromUser: 6,
					toUser: 1,
					content: "test",
					companyName: "test",
				},
			],
			toJSON: function () {
				return this;
			},
		};

		jest.spyOn(User, "findByPk").mockResolvedValueOnce(expectedCV as any);
		const cacheService = new mockCacheService();
		cacheService.getPair.mockResolvedValueOnce(null);
		const mockSetCache = cacheService.setPair.mockResolvedValue(true);

		const result = await UserService.getUserCV(1, cacheService);

		expect(result).toStrictEqual(expectedCV);
		expect(mockSetCache).toBeCalledWith(
			expectedCV.id.toString(),
			expectedCV.toJSON()
		);
	});

	it("should encrypt and compare passwords properly", async () => {
		const password = "testPassword";

		const hashedPassword = await UserService.hashPassword(password);

		const equals = await UserService.comparePasswords(password, hashedPassword);

		expect(equals).toBe(true);
	});
});
