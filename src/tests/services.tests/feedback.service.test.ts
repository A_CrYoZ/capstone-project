import { FeedbackService } from "../../services/feedback.service";
import { Feedback } from "../../models/feedback.model";
import { loadModels } from "../../loaders/models";
import mockCacheService, {
	deletePairMock,
	getPairMock,
	setPairMock,
} from "../__mocks__/cache.service.mock";
import { Sequelize } from "sequelize";
import { BadRequest, NotFound } from "http-errors";

jest.mock("../../services/cache.service", () => {
	return mockCacheService;
});

const sequelize = new Sequelize({
	dialect: "mysql",
	database: "your_database_name",
	username: "your_username",
	password: "your_password",
	host: "localhost",
	port: 5432,
});

loadModels(sequelize);

describe("Feedback Service", () => {
	beforeEach(() => {
		// Clear all instances and calls to constructor and all methods:
		mockCacheService.mockClear();
		getPairMock.mockClear();
		deletePairMock.mockClear();
		setPairMock.mockClear();
	});

	it("should create and return Feedback and clear cache by toUser", async () => {
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		jest.spyOn(Feedback, "create").mockImplementationOnce((feedbackOptions) => {
			return Feedback.build({
				...(feedbackOptions as any),
				id: 1,
			});
		});

		const date = new Date();
		const result = await FeedbackService.createFeedback(
			1,
			2,
			"test",
			"test",
			cacheService
		);

		expect(result).toBeInstanceOf(Feedback);
		expect(result.toJSON()).toStrictEqual({
			fromUser: 1,
			toUser: 2,
			companyName: "test",
			content: "test",
			id: 1,
		});
		expect(mockRedis).toBeCalledWith(result.toUser.toString());
	});

	it("should throw 400 error if provided invalid toUser or fromUser on creation", async () => {
		const cacheService = new mockCacheService();

		jest.spyOn(Feedback, "create").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});

		const date = new Date();

		expect(async () =>
			FeedbackService.createFeedback(1, 2, "test", "test", cacheService)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on creation", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Feedback, "create")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		const date = new Date();
		expect(async () =>
			FeedbackService.createFeedback(1, 2, "test", "test", cacheService)
		).rejects.toThrowError("Something went wrong");
	});

	it("should update return updated feedback", async () => {
		const date = new Date();
		const expectedFeedback = Feedback.build({
			fromUser: 1,
			toUser: 2,
			companyName: "test",
			content: "test",
			id: 1,
		});

		jest.spyOn(Feedback, "update").mockResolvedValueOnce([1]);
		jest.spyOn(Feedback, "findByPk").mockResolvedValueOnce(expectedFeedback);
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const result = await FeedbackService.updateFeedback(
			1,
			1,
			2,
			"test",
			"test",
			cacheService
		);

		expect(result).toBeInstanceOf(Feedback);
		expect(result?.toJSON()).toStrictEqual(expectedFeedback.toJSON());
		expect(mockRedis).toBeCalledWith(expectedFeedback.toUser.toString());
	});

	it("should throw 404 if not found on update", async () => {
		jest.spyOn(Feedback, "update").mockResolvedValueOnce([0]);
		const cacheService = new mockCacheService();

		const date = new Date();

		expect(
			async () =>
				await FeedbackService.updateFeedback(
					1,
					1,
					2,
					"test",
					"test",
					cacheService
				)
		).rejects.toThrowError(NotFound);
	});

	it("should throw 400 error if provided invalid userId on update", async () => {
		jest.spyOn(Feedback, "update").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});
		const cacheService = new mockCacheService();

		const date = new Date();

		expect(
			async () =>
				await FeedbackService.updateFeedback(
					1,
					1,
					2,
					"test",
					"test",
					cacheService
				)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on update", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Feedback, "update")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		const date = new Date();

		expect(async () =>
			FeedbackService.updateFeedback(1, 1, 2, "test", "test", cacheService)
		).rejects.toThrowError("Something went wrong");
	});

	it("should delete feedback", async () => {
		const date = new Date();

		const feedbackProps = {
			fromUser: 1,
			toUser: 2,
			companyName: "test",
			content: "test",
			id: 1,
		};
		const deleteMock = jest.spyOn(Feedback, "destroy").mockResolvedValueOnce(1);
		jest
			.spyOn(Feedback, "findByPk")
			.mockResolvedValueOnce(Feedback.build(feedbackProps));

		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const res = await FeedbackService.deleteFeedbackById(1, cacheService);

		expect(deleteMock).toHaveBeenCalled();
		expect(res).toStrictEqual(true);
		expect(mockRedis).toBeCalledWith(feedbackProps.toUser.toString());
	});

	it("should be 404 on feedback deletion if not found", async () => {
		jest.spyOn(Feedback, "findByPk").mockResolvedValueOnce(null);
		const cacheService = new mockCacheService();

		expect(
			async () => await FeedbackService.deleteFeedbackById(1, cacheService)
		).rejects.toThrowError(NotFound);
	});

	it("should return array of feedbacks", async () => {
		const date = new Date();

		const expectedObj = {
			rows: [
				Feedback.build({
					fromUser: 1,
					toUser: 2,
					companyName: "test",
					content: "test",
					id: 1,
				}),
				Feedback.build({
					fromUser: 2,
					toUser: 1,
					companyName: "test1",
					content: "test1",
					id: 2,
				}),
			],
			count: 2,
		};
		jest
			.spyOn(Feedback, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await FeedbackService.listAllFeedbacks(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});

	it("should return error array if there is no feedbacks", async () => {
		const expectedObj = {
			rows: [],
			count: 0,
		};
		jest
			.spyOn(Feedback, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await FeedbackService.listAllFeedbacks(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});
});
