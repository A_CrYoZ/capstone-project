import { ExperienceService } from "../../services/experience.service";
import { Experience } from "../../models/experience.model";
import { loadModels } from "../../loaders/models";
import mockCacheService, {
	deletePairMock,
	getPairMock,
	setPairMock,
} from "../__mocks__/cache.service.mock";
import { Sequelize } from "sequelize";
import { BadRequest, NotFound } from "http-errors";

jest.mock("../../services/cache.service", () => {
	return mockCacheService;
});

const sequelize = new Sequelize({
	dialect: "mysql",
	database: "your_database_name",
	username: "your_username",
	password: "your_password",
	host: "localhost",
	port: 5432,
});

loadModels(sequelize);

describe("Experiences Service", () => {
	beforeEach(() => {
		// Clear all instances and calls to constructor and all methods:
		mockCacheService.mockClear();
		getPairMock.mockClear();
		deletePairMock.mockClear();
		setPairMock.mockClear();
	});

	it("should create and return Experience and clear cache by userId", async () => {
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		jest
			.spyOn(Experience, "create")
			.mockImplementationOnce((experienceOptions) => {
				return Experience.build({
					...(experienceOptions as any),
					id: 1,
				});
			});

		const date = new Date();
		const result = await ExperienceService.createExperience(
			1,
			"test",
			"test",
			date,
			date,
			"test",
			cacheService
		);

		expect(result).toBeInstanceOf(Experience);
		expect(result.toJSON()).toStrictEqual({
			userId: 1,
			companyName: "test",
			role: "test",
			startDate: date,
			endDate: date,
			description: "test",
			id: 1,
		});
		expect(mockRedis).toBeCalledWith(result.userId.toString());
	});

	it("should throw 400 error if provided invalid userId on creation", async () => {
		const cacheService = new mockCacheService();

		jest.spyOn(Experience, "create").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});

		const date = new Date();

		expect(async () =>
			ExperienceService.createExperience(
				1,
				"test",
				"test",
				date,
				date,
				"test",
				cacheService
			)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on creation", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Experience, "create")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		const date = new Date();
		expect(async () =>
			ExperienceService.createExperience(
				1,
				"test",
				"test",
				date,
				date,
				"test",
				cacheService
			)
		).rejects.toThrowError("Something went wrong");
	});

	it("should update return updated experience", async () => {
		const date = new Date();
		const expectedExp = Experience.build({
			userId: 1,
			companyName: "test",
			role: "test",
			startDate: date,
			endDate: date,
			description: "test",
			id: 1,
		});

		jest.spyOn(Experience, "update").mockResolvedValueOnce([1]);
		jest.spyOn(Experience, "findByPk").mockResolvedValueOnce(expectedExp);
		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const result = await ExperienceService.updateExperience(
			1,
			1,
			"test",
			"test",
			date,
			date,
			"test",
			cacheService
		);

		expect(result).toBeInstanceOf(Experience);
		expect(result?.toJSON()).toStrictEqual(expectedExp.toJSON());
		expect(mockRedis).toBeCalledWith(expectedExp.userId.toString());
	});

	it("should throw 404 if not found on update", async () => {
		jest.spyOn(Experience, "update").mockResolvedValueOnce([0]);
		const cacheService = new mockCacheService();

		const date = new Date();

		expect(
			async () =>
				await ExperienceService.updateExperience(
					1,
					1,
					"test",
					"test",
					date,
					date,
					"test",
					cacheService
				)
		).rejects.toThrowError(NotFound);
	});

	it("should throw 400 error if provided invalid userId on update", async () => {
		jest.spyOn(Experience, "update").mockRejectedValueOnce({
			parent: {
				errno: 1452,
			},
		});
		const cacheService = new mockCacheService();

		const date = new Date();

		expect(
			async () =>
				await ExperienceService.updateExperience(
					1,
					1,
					"test",
					"test",
					date,
					date,
					"test",
					cacheService
				)
		).rejects.toThrowError(BadRequest);
	});

	it("should throw error if something goes wrong on update", async () => {
		const cacheService = new mockCacheService();

		jest
			.spyOn(Experience, "update")
			.mockRejectedValueOnce(new Error("Something went wrong"));

		const date = new Date();

		expect(async () =>
			ExperienceService.updateExperience(
				1,
				1,
				"test",
				"test",
				date,
				date,
				"test",
				cacheService
			)
		).rejects.toThrowError("Something went wrong");
	});

	it("should delete experience", async () => {
		const date = new Date();

		const experienceProps = {
			userId: 1,
			companyName: "test",
			role: "test",
			startDate: date,
			endDate: date,
			description: "test",
			id: 1,
		};
		const deleteMock = jest
			.spyOn(Experience, "destroy")
			.mockResolvedValueOnce(1);
		jest
			.spyOn(Experience, "findByPk")
			.mockResolvedValueOnce(Experience.build(experienceProps));

		const cacheService = new mockCacheService();
		const mockRedis = cacheService.deletePair.mockResolvedValueOnce(true);

		const res = await ExperienceService.deleteExperienceById(1, cacheService);

		expect(deleteMock).toHaveBeenCalled();
		expect(res).toStrictEqual(true);
		expect(mockRedis).toBeCalledWith(experienceProps.userId.toString());
	});

	it("should be false on experience deletion if not found", async () => {
		jest.spyOn(Experience, "findByPk").mockResolvedValueOnce(null);
		const cacheService = new mockCacheService();

		expect(
			async () => await ExperienceService.deleteExperienceById(1, cacheService)
		).rejects.toThrowError(NotFound);
	});

	it("should return array of experiences", async () => {
		const date = new Date();

		const expectedObj = {
			rows: [
				Experience.build({
					userId: 1,
					companyName: "test1",
					role: "test1",
					startDate: date,
					endDate: date,
					description: "test1",
					id: 1,
				}),
				Experience.build({
					userId: 2,
					companyName: "test2",
					role: "test2",
					startDate: date,
					endDate: date,
					description: "test2",
					id: 2,
				}),
			],
			count: 2,
		};
		jest
			.spyOn(Experience, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await ExperienceService.listAllExperiences(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});

	it("should return empty array if there is no experiences", async () => {
		const expectedObj = {
			rows: [],
			count: 0,
		};
		jest
			.spyOn(Experience, "findAndCountAll")
			.mockResolvedValueOnce(expectedObj as any);

		const res = await ExperienceService.listAllExperiences(2, 0);

		expect(res).toStrictEqual(expectedObj);
	});
});
