import supertest from "supertest";
import http from "http";
import { loadApp } from "../../loaders/app";
import { logger } from "../../libs/logger";
import path from "path";

let server;

let userOwnerId: number;

let adminToken: string;

const genericUser: {
	id: number;
	token: string;
} = {
	id: 0,
	token: "",
};

beforeAll(async () => {
	const app = await loadApp();
	server = http.createServer(app);
	server.listen(3001, () =>
		logger.info(`Application is running on http://localhost:3001`)
	);
});

describe("Auth test", () => {
	it("should return 400 if provided incorrect body", async () => {
		await supertest(server)
			.post("/api/auth/register")
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", "Test")
			.field("lastName", "Test")
			.field("email", "asdas")
			.expect(400);

		await supertest(server)
			.post("/api/auth/register")
			.field("firstName", "Test")
			.field("lastName", "Test")
			.field("title", "Test")
			.field("summary", "Test")
			.field("email", "user@testmail.com")
			.field("password", "u`TNC_(<8z")
			.expect(400);
	});

	it("should register user and return 201 status code", async () => {
		const res = await supertest(server)
			.post("/api/auth/register")
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", "Test")
			.field("lastName", "Test")
			.field("title", "test")
			.field("summary", "summary")
			.field("email", "user@testmail.com")
			.field("password", "u`TNC_(<8z")
			.expect(201);

		const resGeneric = await supertest(server)
			.post("/api/auth/register")
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", "GenericTest")
			.field("lastName", "GenericTest")
			.field("title", "test")
			.field("summary", "summary")
			.field("email", "GenericTest@testmail.com")
			.field("password", "u`TNC_(<8z")
			.expect(201);

		userOwnerId = res.body.id;

		genericUser.id = resGeneric.body.id;
	});

	it("should return 400 if provided incorrect data for login", async () => {
		await supertest(server)
			.post("/api/auth/login")
			.send({
				email: "user@testmail.com",
				password: "asdasdas",
			})
			.expect(400);
	});

	it("should return 200 and token if login success", async () => {
		const resGeneric = await supertest(server)
			.post("/api/auth/login")
			.send({
				email: "GenericTest@testmail.com",
				password: "u`TNC_(<8z",
			})
			.expect(200);

		expect(resGeneric.body.token).toBeDefined();

		genericUser.token = resGeneric.body.token;
	});
});

describe("Users test", () => {
	let userToken: string;
	let createdUserId: number;

	const userData = {
		firstName: "Arnold",
		lastName: "De Lorian",
		title: "Some title",
		summary: "summary",
		email: "somemail123@testmail.com",
		password: "A123@fqwe_da-sds",
		role: "User",
	};

	beforeAll(async () => {
		const admin = await supertest(server)
			.post("/api/auth/login")
			.send({
				email: "vtsoyer@gmail.com",
				password: "u`TNC_(<8z",
			})
			.expect(200);

		expect(admin.body.token).toBeDefined();

		adminToken = admin.body.token;

		const user = await supertest(server)
			.post("/api/auth/login")
			.send({
				email: "user@testmail.com",
				password: "u`TNC_(<8z",
			})
			.expect(200);

		expect(user.body.token).toBeDefined();

		userToken = user.body.token;
	});

	it("should create user and return it if logged in as admin", async () => {
		const { firstName, lastName, title, summary, email, password, role } =
			userData;

		const res = await supertest(server)
			.post("/api/users")
			.set("Authorization", `Bearer ${adminToken}`)
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", firstName)
			.field("lastName", lastName)
			.field("title", title)
			.field("summary", summary)
			.field("email", email)
			.field("password", password)
			.field("role", role)
			.expect(201);

		expect(res.body.firstName).toStrictEqual(firstName);
		expect(res.body.lastName).toStrictEqual(lastName);
		expect(res.body.title).toStrictEqual(title);
		expect(res.body.summary).toStrictEqual(summary);
		expect(res.body.email).toStrictEqual(email);
		expect(res.body.role).toStrictEqual(role);

		createdUserId = res.body.id;
	});

	it("shouldn't create user, return 400 if provided incorrect body and logged in as admin", async () => {
		const email = "sdlkals.casok.com";
		const { firstName, lastName, title, summary, password, role } = userData;

		await supertest(server)
			.post("/api/users")
			.set("Authorization", `Bearer ${adminToken}`)
			.field("firstName", firstName)
			.field("lastName", lastName)
			.field("title", title)
			.field("summary", summary)
			.field("email", email)
			.field("password", password)
			.field("role", role)
			.expect(400);
	});

	it("shouldn't create user, return 401 if logged in as user", async () => {
		const { firstName, lastName, title, summary, email, password, role } =
			userData;

		await supertest(server)
			.post("/api/users")
			.set("Authorization", `Bearer ${userToken}`)
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", firstName)
			.field("lastName", lastName)
			.field("title", title)
			.field("summary", summary)
			.field("email", email)
			.field("password", password)
			.field("role", role)
			.expect(401);
	});

	it("should GET list of users if logged as admin", async () => {
		const res = await supertest(server)
			.get("/api/users?pageSize=3&page=0")
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(200);

		expect(res.body).toBeInstanceOf(Array);
		expect(res.header["x-total-count"]).toBeDefined();

		await supertest(server)
			.get("/api/users?pageSize=3&page=0")
			.set("Authorization", `Bearer ${userToken}`)
			.expect(401);
	});

	it("should get user by id or 404 if not found", async () => {
		await supertest(server).get("/api/users/1").expect(200);
		await supertest(server).get("/api/users/421341").expect(404);
	});

	it("should get user CV by id or 404 if not found", async () => {
		const res = await supertest(server)
			.get(`/api/users/${createdUserId}/cv`)
			.expect(200);

		const { firstName, lastName, title, summary, email, password, role } =
			userData;

		expect(res.body.firstName).toStrictEqual(firstName);
		expect(res.body.lastName).toStrictEqual(lastName);
		expect(res.body.title).toStrictEqual(title);
		expect(res.body.summary).toStrictEqual(summary);
		expect(res.body.email).toStrictEqual(email);
		expect(res.body.experiences).toBeDefined();
		expect(res.body.experiences).toBeInstanceOf(Array);
		expect(res.body.projects).toBeDefined();
		expect(res.body.projects).toBeInstanceOf(Array);
		expect(res.body.feedbacks).toBeDefined();
		expect(res.body.feedbacks).toBeInstanceOf(Array);

		await supertest(server).get("/api/users/421341/cv").expect(404);
	});

	it("should update user if logged in as owner and return 401 if not", async () => {
		const { lastName, title, summary, password, role } = userData;

		const newName = "NewName";
		const newMail = "newmail@test.com";

		const res = await supertest(server)
			.put(`/api/users/${userOwnerId}`)
			.set("Authorization", `Bearer ${userToken}`)
			.attach("avatar", path.join(__dirname, "./someImage.jpg"))
			.field("firstName", newName)
			.field("lastName", lastName)
			.field("title", title)
			.field("summary", summary)
			.field("email", newMail)
			.field("password", password)
			.field("role", role)
			.expect(200);

		expect(res.body.firstName).toStrictEqual(newName);
		expect(res.body.lastName).toStrictEqual(lastName);
		expect(res.body.title).toStrictEqual(title);
		expect(res.body.summary).toStrictEqual(summary);
		expect(res.body.email).toStrictEqual(newMail);
		expect(res.body.role).toStrictEqual(role);

		await supertest(server)
			.put(`/api/users/${userOwnerId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.field("firstName", newName)
			.field("lastName", lastName)
			.field("title", title)
			.field("summary", summary)
			.field("email", newMail)
			.field("password", password)
			.field("role", role)
			.expect(401);
	});

	it("should delete user and return 401 if not owner or admin or 404 if not found", async () => {
		const resTmpToken = await supertest(server)
			.post("/api/auth/login")
			.send({
				email: userData.email,
				password: userData.password,
			})
			.expect(200);

		const tmpToken = resTmpToken.body.token;

		await supertest(server)
			.del(`/api/users/${createdUserId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(204);

		await supertest(server)
			.del(`/api/users/${createdUserId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(404);

		await supertest(server)
			.del(`/api/users/${userOwnerId}`)
			.set("Authorization", `Bearer ${tmpToken}`)
			.expect(401);

		await supertest(server)
			.del(`/api/users/${userOwnerId}`)
			.set("Authorization", `Bearer dasdasd`)
			.expect(401);

		await supertest(server)
			.del(`/api/users/${userOwnerId}`)
			.set("Authorization", `Bearer ${userToken}`)
			.expect(204);
	});

	it("should return 404 on deletion if user not found", async () => {
		await supertest(server)
			.del(`/api/users/2141231`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(404);
	});
});

describe("Experiences test", () => {
	let expId: number;

	const testExpBody = {
		userId: 1,
		companyName: "test",
		role: "test",
		startDate: "2019-02-01",
		endDate: "2025-02-01",
		description: "test description",
	};

	it("should create experience", async () => {
		const { companyName, description, role, userId } = testExpBody;
		const res = await supertest(server)
			.post("/api/experience")
			.set("Authorization", `Bearer ${adminToken}`)
			.send(testExpBody)
			.expect(201);

		expect(res.body.companyName).toStrictEqual(companyName);
		expect(res.body.description).toStrictEqual(description);
		expect(res.body.userId).toStrictEqual(userId);
		expect(res.body.endDate).toBeDefined();
		expect(res.body.startDate).toBeDefined();
		expect(res.body.role).toStrictEqual(role);

		expId = res.body.id;
	});

	it("should return list of experiences if logged in as admin and 401 if not", async () => {
		const res = await supertest(server)
			.get("/api/experience?pageSize=3&page=0")
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(200);

		expect(res.body).toBeInstanceOf(Array);
		expect(res.header["x-total-count"]).toBeDefined();

		await supertest(server)
			.get("/api/experience?pageSize=3&page=0")
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);
	});

	it("should get experience by id or 404 if not found", async () => {
		await supertest(server).get("/api/experience/1").expect(200);
		await supertest(server).get("/api/experience/421341").expect(404);
	});

	it("should update experience if logged in as owner or admin and return 401 if not, 404 if not found", async () => {
		const { companyName, role, userId } = testExpBody;
		const newDescription = "newDescription";

		const res = await supertest(server)
			.put(`/api/experience/${expId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ ...testExpBody, description: newDescription })
			.expect(200);

		expect(res.body.companyName).toStrictEqual(companyName);
		expect(res.body.startDate).toBeDefined();
		expect(res.body.endDate).toBeDefined();
		expect(res.body.description).toStrictEqual(newDescription);
		expect(res.body.userId).toStrictEqual(userId);
		expect(res.body.role).toStrictEqual(role);

		await supertest(server)
			.put(`/api/experience/${expId}`)
			.set("Authorization", `Bearer ${genericUser.token}`)
			.send({ ...testExpBody, description: newDescription })
			.expect(401);

		await supertest(server)
			.put(`/api/experience/1412312`)
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ ...testExpBody, description: newDescription })
			.expect(404);
	});

	it("should delete experience and return 401 if not owner or admin", async () => {
		await supertest(server)
			.del(`/api/experience/${expId}`)
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);

		await supertest(server)
			.del(`/api/experience/${expId}`)
			.set("Authorization", `Bearer dasdasd`)
			.expect(401);

		await supertest(server)
			.del(`/api/experience/${expId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(204);
	});
});

describe("Feedback test", () => {
	let feedbackId: number;

	const testFeedbackBody = {
		fromUser: 1,
		companyName: "test",
		toUser: 1,
		content: "asmdmlasmdlasmd",
	};

	it("should create feedback", async () => {
		const { fromUser, companyName, toUser, content } = testFeedbackBody;
		const res = await supertest(server)
			.post("/api/feedback")
			.set("Authorization", `Bearer ${adminToken}`)
			.send(testFeedbackBody)
			.expect(201);

		expect(res.body.companyName).toStrictEqual(companyName);
		expect(res.body.fromUser).toStrictEqual(fromUser);
		expect(res.body.toUser).toStrictEqual(toUser);
		expect(res.body.content).toStrictEqual(content);

		feedbackId = res.body.id;
	});

	it("should return list of feedbacks if logged in as admin and 401 if not", async () => {
		const res = await supertest(server)
			.get("/api/feedback?pageSize=3&page=0")
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(200);

		expect(res.body).toBeInstanceOf(Array);
		expect(res.header["x-total-count"]).toBeDefined();

		await supertest(server)
			.get("/api/feedback?pageSize=3&page=0")
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);
	});

	it("should get feedback by id or 404 if not found", async () => {
		await supertest(server).get("/api/feedback/1").expect(200);
		await supertest(server).get("/api/feedback/421341").expect(404);
	});

	it("should update feedback if logged in as owner or admin and return 401 if not, 404 if not found", async () => {
		const { fromUser, toUser, content } = testFeedbackBody;
		const newCompanyName = "newCompanyName";

		const res = await supertest(server)
			.put(`/api/feedback/${feedbackId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ ...testFeedbackBody, companyName: newCompanyName })
			.expect(200);

		expect(res.body.companyName).toStrictEqual(newCompanyName);
		expect(res.body.fromUser).toStrictEqual(fromUser);
		expect(res.body.toUser).toStrictEqual(toUser);
		expect(res.body.content).toStrictEqual(content);

		await supertest(server)
			.put(`/api/feedback/${feedbackId}`)
			.set("Authorization", `Bearer ${genericUser.token}`)
			.send({ ...testFeedbackBody, companyName: newCompanyName })
			.expect(401);

		await supertest(server)
			.put(`/api/feedback/1412312`)
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ ...testFeedbackBody, companyName: newCompanyName })
			.expect(404);
	});

	it("should delete feedback and return 401 if not owner or admin", async () => {
		await supertest(server)
			.del(`/api/feedback/${feedbackId}`)
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);

		await supertest(server)
			.del(`/api/feedback/${feedbackId}`)
			.set("Authorization", `Bearer dasdasd`)
			.expect(401);

		await supertest(server)
			.del(`/api/feedback/${feedbackId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(204);
	});
});

describe("Projects test", () => {
	let projId: number;

	const projData = {
		userId: 1,
		description: "test project",
	};

	it("should create projects", async () => {
		const { userId, description } = projData;

		const res = await supertest(server)
			.post("/api/projects")
			.set("Authorization", `Bearer ${adminToken}`)
			.attach("image", path.join(__dirname, "./someImage.jpg"))
			.field("userId", userId)
			.field("description", description)
			.expect(201);

		expect(res.body.description).toStrictEqual(description);
		expect(res.body.userId).toStrictEqual(userId);

		projId = res.body.id;
	});

	it("should return list of projects if logged in as admin and 401 if not", async () => {
		const res = await supertest(server)
			.get("/api/projects?pageSize=3&page=0")
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(200);

		expect(res.body).toBeInstanceOf(Array);
		expect(res.header["x-total-count"]).toBeDefined();

		await supertest(server)
			.get("/api/projects?pageSize=3&page=0")
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);
	});

	it("should get projects by id or 404 if not found", async () => {
		await supertest(server).get(`/api/projects/${projId}`).expect(200);
		await supertest(server).get("/api/projects/421341").expect(404);
	});

	it("should update projects if logged in as owner or admin and return 401 if not, 404 if not found", async () => {
		const { userId } = projData;
		const newDescription = "newDescription";

		const res = await supertest(server)
			.put(`/api/projects/${projId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.attach("image", path.join(__dirname, "./someImage.jpg"))
			.field("userId", userId)
			.field("description", newDescription)
			.expect(200);

		expect(res.body.description).toStrictEqual(newDescription);
		expect(res.body.userId).toStrictEqual(userId);

		await supertest(server)
			.put(`/api/projects/${projId}`)
			.set("Authorization", `Bearer${genericUser.token}`)
			.field("userId", userId)
			.expect(401);

		await supertest(server)
			.put(`/api/projects/1412312`)
			.set("Authorization", `Bearer ${adminToken}`)
			.attach("image", path.join(__dirname, "./someImage.jpg"))
			.field("userId", userId)
			.field("description", newDescription)
			.expect(404);
	});

	it("should delete feedback and return 401 if not owner or admin", async () => {
		await supertest(server)
			.del(`/api/projects/${projId}`)
			.set("Authorization", `Bearer ${genericUser.token}`)
			.expect(401);

		await supertest(server)
			.del(`/api/projects/${projId}`)
			.set("Authorization", `Bearer dasdasd`)
			.expect(401);

		await supertest(server)
			.del(`/api/projects/${projId}`)
			.set("Authorization", `Bearer ${adminToken}`)
			.expect(204);
	});
});

afterAll(async () => {
	await supertest(server)
		.del(`/api/users/${genericUser.id}`)
		.set("Authorization", `Bearer ${genericUser.token}`)
		.expect(204);
	server.close();
});
