export enum DBDialects {
  MYSQL = "mysql",
}

export interface Config {
  db: {
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    dialect: DBDialects;
  };
  redis: {
    host: string;
    port: number;
  };
  auth: {
    secret: string;
  };
}

const configs: {
  development: Config;
  production: Config;
} = {
  development: {
    db: {
      host: "localhost",
      port: 3306,
      username: "dev",
      password: "dev",
      database: "capstone_project",
      dialect: DBDialects.MYSQL,
    },
    redis: {
      host: "localhost",
      port: 6379,
    },
    auth: {
      secret: "some-dev-secret",
    },
  },
  production: {
    db: {
      host: "localhost",
      port: 3306,
      username: "prod",
      password: "prod",
      database: "capstone_project",
      dialect: DBDialects.MYSQL,
    },
    redis: {
      host: "localhost",
      port: 6379,
    },
    auth: {
      secret: "some-dev-secret",
    },
  },
};

const getConfig = (): Config => {
  if (!process.env.NODE_ENV) {
    throw new Error(
      'Env parameter NODE_ENV must be specified! Possible values are "development", ...',
    );
  }

  const env = process.env.NODE_ENV as "development" | "production";

  if (!configs[env]) {
    throw new Error(
      'Unsupported NODE_ENV value was provided! Possible values are "development" and "production"',
    );
  }

  return configs[env];
};

export const config = getConfig();
