import fs from "fs";
import { promisify } from "util";

export async function deleteImage(path: fs.PathLike): Promise<void> {
  const rm = promisify(fs.rm);

  await rm(path);
}
