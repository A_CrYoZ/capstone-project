import { clsProxify } from "cls-proxify";
import pino from "pino";

export const baseLogger = pino({
	prettyPrint: process.env.NODE_ENV === "development",
});

export const logger = clsProxify(baseLogger);
