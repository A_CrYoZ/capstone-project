process.env.NODE_ENV = "development";

module.exports = {
	collectCoverage: true,
	coverageThreshold: {
		global: {
			lines: 75,
		},
	},
};
